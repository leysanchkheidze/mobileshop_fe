const pug = require("pug");

const request = require("request-promise");

let responseJson;

//templates:
const tariffsPugFile = "./views/tariffs.pug";
const compiledFunctionTariffs = pug.compileFile(tariffsPugFile);

const deviceCatalogPugFile = "./views/device_catalog.pug";
const compiledFunctionDeviceCatalog = pug.compileFile(deviceCatalogPugFile);

const startPagePugFile = "./views/start.pug";
const compiledFunctionStart = pug.compileFile(startPagePugFile);

const compatibleDevicesPugFile = "./views/devices_by_tariff.pug";
const compiledFunctionCompatibleDevices = pug.compileFile(
  compatibleDevicesPugFile
);

const orderConfigurationPugFile = "./views/order_configuration.pug";
const compiledFunctionorderConfiguration = pug.compileFile(
  orderConfigurationPugFile
);

const compatibleTariffsPugFile = "./views/tariffs_by_device.pug";
const compiledFunctionCompatibleTariffs = pug.compileFile(
  compatibleTariffsPugFile
);

const basketPagePugFile = "./views/basket.pug";
const compiledFunctionBasket = pug.compileFile(basketPagePugFile);

const personalDataPagePugFile = "./views/personal_data.pug";
const compiledFunctionPersonalData = pug.compileFile(personalDataPagePugFile);

const addressPagePugFile = "./views/address.pug";
const compiledFunctionAddress = pug.compileFile(addressPagePugFile);

const passportPagePugFile = "./views/passport.pug";
const compiledFunctionPassport = pug.compileFile(passportPagePugFile);

const finalizationPagePugFile = "./views/order_finalization.pug";
const compiledFunctionFinalPage = pug.compileFile(finalizationPagePugFile);

const confrimationPagePugFile = "./views/order_confirmation.pug";
const compiledFunctionConfirmation = pug.compileFile(confrimationPagePugFile);



//admin request endpoints:
const adminUser = "shop";
const adminPass = "shop123";
const adminServer = "http://localhost";
const adminPort = "8080";
const adminForShopPath = "/bcc/forshop";
const adminGetAllTariffsPath = "/tariffs/all";
const adminGetAllDevicesPath = "/devices/all";
const adminCompatibleOffersByTariffPath = "/offers/bytariff/";
const adminOrderConfigurationViewPath = "/offers/configuration/";
const adminCompatibleOffersByDevicePath = "/offers/bydevice/";
const adminNewOrderPath = "/orders/";
const adminPostPersonalDataPath = "/orders/personaldata/";
const adminPostAddressDataPath = "/orders/address/";
const adminGetPassportTypesPath = "/orders/passport_types/";
const adminPostPassportDataPath = "/orders/passport/";
const adminPostOrderConfirmationPath = "/orders/confirmation/";


exports.getTariffsPage = function (req, res) {
  console.log("Got get request for tariffs page");
  openPage(
    res,
    compiledFunctionTariffs,
    getRequestOptions(adminGetAllTariffsPath)
  );
};

exports.getDevicesPage = function (req, res) {
  console.log("Got get request for devices page");
  openPage(
    res,
    compiledFunctionDeviceCatalog,
    getRequestOptions(adminGetAllDevicesPath)
  );
};

exports.getStartPage = function (req, res) {
  console.log("got get request for: start page");
  sendPage(res, compiledFunctionStart);
};

exports.getDevicesByTariffPage = function (req, res) {
  console.log("got get request for devices by tariff page");
  console.log("tariff id = " + req.params.tagId);
  let tagId = req.params.tagId;

  openPage(
    res,
    compiledFunctionCompatibleDevices,
    getRequestOptions(adminCompatibleOffersByTariffPath + tagId)
  );
};

exports.getConfigurationPage = function (req, res) {
  console.log("got get request for configuration page");
  console.log("offer id = " + req.params.tagId);

  let tagId = req.params.tagId;

  try {
    openPage(
      res,
      compiledFunctionorderConfiguration,
      getRequestOptions(adminOrderConfigurationViewPath + tagId)
    );
  } catch (err) {
    res.send(
      compiledFunction({
        responseJson: { error: true },
      })
    );
  }
};

exports.getTariffsByDevicePage = function (req, res) {
  console.log("got get request for tariffs compatible to device");

  console.log("tariff id = " + req.params.tagId);
  let tagId = req.params.tagId;

  openPage(
    res,
    compiledFunctionCompatibleTariffs,
    getRequestOptions(adminCompatibleOffersByDevicePath + tagId)
  );
};

exports.postToBasketPage = function (req, res) {
  console.log("got post request for basket page");
  console.log("!!!req.body = ");
  console.log(req.body);
  const body = req.body;

  const adminRequestBody = createPostBasketBody(body);

  openPage(
    res,
    compiledFunctionBasket,
    postRequestOptions(adminNewOrderPath, adminRequestBody)
  );
};

exports.getPersonalDataPage = function (req, res) {
  console.log("got get request for personal data page");
  // res.setHeader("set-cookie", ["cookie2=value2; SameSite=None; Secure"]);

  sendPage(res, compiledFunctionPersonalData);
};

exports.postToAddressPage = function (req, res) {
  console.log("got post request to address page");
  const body = req.body;

  openPage(
    res,
    compiledFunctionAddress,
    postRequestOptions(adminPostPersonalDataPath, body)
  );
};

exports.postToPassportPage = function (req, res) {
  console.log("got post request to passport page");
  const body = req.body;
  openPassportPage(
    req,
    res,
    compiledFunctionPassport,
    getRequestOptions(adminGetPassportTypesPath),
    postRequestOptions(adminPostAddressDataPath, body)
  );
};

exports.postToConfirmationPage = function(req, res) {
    console.log("got post request to confirmation page");
    const body = req.body;
  
    openPage(
      res,
      compiledFunctionConfirmation,
      postRequestOptions(adminPostPassportDataPath, body)
    );
};

exports.postToFinalPage = function(req, res) {
    console.log("got post request to order finalization");
    const body = req.body;
    let backendPayload = createPostToFinalBody(body);
  
    openPage(
      res,
      compiledFunctionFinalPage,
      postRequestOptions(adminPostOrderConfirmationPath, backendPayload)
    ); 
};















function createPostToFinalBody(initialRequestBody) {
    let confirmation = initialRequestBody.confirmation;
  
    if (confirmation === "true") {
      return {
        confirmation: true,
      };
    } else {
      return {
        confirmation: false,
      };
    }
  };

async function getDataFromBackend(options) {
    let response = await promisifiedRequest(options);
  
    if (response.statusCode != 200) {
      console.log("Response status code " + response.statusCode);
      responseJson = {
        customError: "Response status code " + response.statusCode,
      };
    } else {
      try {
        responseJson = JSON.parse(response.body);
      } catch (e) {
        responseJson = response.body;
      }
    }

    return responseJson;
  }

const openPassportPage = async (
  req,
  res,
  compiledFunction,
  getPassportTypeOptions,
  postOptions
) => {
  let passportTypes = await getDataFromBackend(getPassportTypeOptions);
  console.log("Received passport types:")
  console.log(passportTypes);
  const result = await callBackend(postOptions);
  console.log(responseJson);
  const body = req.body;
  sendPassportPage(
    res,
    compiledFunctionPassport,
    passportTypes
  );
};

function sendPassportPage(res, compiledFunction, receivedData) {
  res.send(
    compiledFunction({
      responseJson: responseJson,
      passportTypes: receivedData,
    })
  );
};

function postRequestOptions(requestPath, requestBody) {
  return {
    url: adminServer + ":" + adminPort + adminForShopPath + requestPath,
    auth: {
      username: adminUser,
      password: adminPass,
    },
    method: "POST",
    headers: {
      Accept: "application/json",
      "Accept-Charset": "utf-8",
      "User-Agent": "mobile-shop",
    },
    json: requestBody,
  };
}

function createPostBasketBody(initialJson) {
  let offer = initialJson.offerID;
  let tariff = initialJson.tariffID;
  let device = initialJson.deviceID;
  let options = [];

  for (const [key, value] of Object.entries(initialJson)) {
    if (key.startsWith("option-")) {
      options.push(value);
    }
  }

  return {
    offerID: offer,
    tariffID: tariff,
    deviceID: device,
    optionIDs: options,
  };
}

let promisifiedRequest = function (options) {
  return new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      if (response) {
        return resolve(response);
      }
      if (error) {
        return reject(error);
      }
    });
  });
};

function getRequestOptions(requestPath) {
  return {
    url: adminServer + ":" + adminPort + adminForShopPath + requestPath,
    auth: {
      username: adminUser,
      password: adminPass,
    },
    method: "GET",
    headers: {
      Accept: "application/json",
      "Accept-Charset": "utf-8",
      "User-Agent": "mobile-shop",
    },
  };
}

async function callBackend(options) {
  let response = await promisifiedRequest(options);

  if (response.statusCode != 200) {
    console.log("Response status code " + response.statusCode);
    responseJson = {
      customError: "Response status code " + response.statusCode,
    };
  } else {
    try {
      responseJson = JSON.parse(response.body);
    } catch (e) {
      responseJson = response.body;
    }
  }
}

function sendPage(res, compiledFunction) {
  res.send(
    compiledFunction({
      responseJson: responseJson,
    })
  );
}

const openPage = async (res, compiledFunction, options) => {
  const result = await callBackend(options);
  console.log(responseJson);
  sendPage(res, compiledFunction);
};
