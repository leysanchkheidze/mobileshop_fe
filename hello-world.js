//TODO's:
//handle error connection to backend - now FE just stops

var shopRouter = require('./routes/shop');

var express = require("express");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
const { body, validationResult } = require("express-validator");
const port = 8888;



var app = express();
app.use(express.static("public"));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/shop', shopRouter);


app.get("/index.htm", function (req, res) {
  //не логируется ничего, хотя в app.get("/hello" логируется
  console.log("got get request for index.htm");
  console.log("Cookies: ", req.cookies);
  console.log("going to open file " + __dirname + "/" + "index.htm");
  res.sendFile(__dirname + "/" + "index.htm");
});

app.get("*", function (req, res) {
  res.status(404).send("what???");
});

var server = app.listen(port, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log("Mobile shop app listening at http://%s:%s", host, port);
});
