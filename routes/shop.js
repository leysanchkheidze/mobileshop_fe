var express = require("express");
var router = express.Router();

//urls of shop pages:
const startPage = "/";
const tariffCatalogPage = "/tariffs";
const deviceCatalogPage = "/devices";
const compatibleDevicesPage = "/devices/bytariff/";
const orderConfigurationPage = "/order/configuration/";
const compatibleTariffsPage = "/tariffs/bydevice/";
const basketPage = "/order/basket";
const personalDataPage = "/order/personaldata";
const addressPage = "/order/toaddress";
const passportPage = "/order/topassport";
const confirmationPage = "/order/toconfirmation";
const finalPage = "/order/tofinal";



//controllers:
var shop_controller = require("../controllers/shopController");




// get /shop/tariffs
router.get(tariffCatalogPage, shop_controller.getTariffsPage);

//get http://localhost:8888/shop/devices
router.get(deviceCatalogPage, shop_controller.getDevicesPage);

//get start page /
router.get(startPage, shop_controller.getStartPage);

//get /shop//devices/bytariff/{id}
router.get(compatibleDevicesPage + ":tagId", shop_controller.getDevicesByTariffPage);

//get http://localhost:8888/shop/order/configuration/{offerID}
router.get(orderConfigurationPage + ":tagId", shop_controller.getConfigurationPage);

// get get http://localhost:8888/shop/tariffs/bydevice/{id}
router.get(compatibleTariffsPage + ":tagId", shop_controller.getTariffsByDevicePage);

////post /shop/order/tobasket
router.post(basketPage, shop_controller.postToBasketPage);

//get http://localhost:8888/shop/order/personaldata
router.get(personalDataPage, shop_controller.getPersonalDataPage);

//post /shop/order/toaddress
router.post(addressPage, shop_controller.postToAddressPage);

//post /shop/order/topassport
router.post(passportPage, shop_controller.postToPassportPage);

//post /shop/order/toconfirmation
router.post(confirmationPage, shop_controller.postToConfirmationPage);

//post /order/tofinal
router.post(finalPage, shop_controller.postToFinalPage);





module.exports = router;
